<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'B|{hZ)=5vRsyX`B#>I#Prq=0l;myW%5Gzmja?p)Vh)LAOh2zH9S+ 7N@.fc~JGqu' );
define( 'SECURE_AUTH_KEY',  'q|-)~:c;wAcyGy*J?+.h{%8LFo%%y!b1D`1GYaKWsD#Dgh8~A#v9T LdtJzR`9wV' );
define( 'LOGGED_IN_KEY',    ']s3awdCWcz,fhkohs2*2q,)K!{zqg4gr6v@g;m7rYEcY30GKG,`y4K`-!56fF~Af' );
define( 'NONCE_KEY',        'YnHZsfhFY.?C];o!dEHL~wU%4w/pM YI:!,6ipztGOSp6F}pJGg3|jaN*}E~Ke c' );
define( 'AUTH_SALT',        '[0Z;YkD|{!KJ;vKq<49Na>cCce%TXxSz/PB{ez/Q6$6_G)JoIc7bO[ia~6{Rgo2C' );
define( 'SECURE_AUTH_SALT', 'I[0YcVFr{R.!hNHY;nw)T7i}c}x?^&Z:mUf(wea|K]J8Vr{X{U%l<dF!@VQaBlfd' );
define( 'LOGGED_IN_SALT',   '510M~lL9!^iy!5?xQLw]nV-dmT3<RZ3[el;lMaz=vcTL$W+Ln-h&Ce_H0Y,$EsU!' );
define( 'NONCE_SALT',       '5/=r)-|{`8Dw@mT<J4t?ybg0>3ZYDm`p2qQRc{*uUH91_d%JNlW/sUB~7 R3su1:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
